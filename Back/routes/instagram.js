var express = require('express');
var app = express();
var ig = require('instagram-node').instagram();

//location of our static files(css,js,etc..)
app.use(express.static(__dirname + '/public'));

//set the view engine to use ejs
app.set('view engine', 'ejs');

ig.use({
  client_id:    'e6d1910213934fceb9c42b20b770ebf2',
client_secret: '763b45bdcd9343f98ab2fda0234abafc'
});

//the redirect uri we set when registering our application
var redirectUri = 'http://auriccircle.org:3000/instagram/handleAuth';
var redirectUri2 = 'http://auriccircle.org:3000/instagram';

app.get('/authorize', function(req, res){
    // set the scope of our application to be able to access likes and public content
    console.log("auth1");
    res.redirect(ig.get_authorization_url(redirectUri) );
});
app.get('/handleAuth', function(req, res){
    //retrieves the code that was passed along as a query to the '/handleAuth' route and uses this code to construct an access token
    console.log("auth2");
    ig.authorize_user(req.query.code, redirectUri, function(err, result){
    
        if(err) res.send( err );
    // store this access_token in a global variable called accessToken
        accessToken = result.access_token;
    console.log(accessToken);

    // After getting the access_token redirect to the '/' route 
        res.redirect(redirectUri2);
    });
})
app.get('/', function(req, res){
   // create a new instance of the use method which contains the access token gotten
    ig.use({
     access_token : accessToken
    });
console.log("auth3");
    ig.user_media_recent(access_token.split('.')[0], function(err, result, pagination, remaining, limit) {
        if(err) res.json("err");
        console.log("auth4");
    //  pass the json file gotten to our ejs template
      //  res.render('pages/index', { instagram : result });
    });
console.log("auth5");
res.redirect("auriccircle.com/auriccircle/Front");
});

//app.listen(3000);

module.exports = app;
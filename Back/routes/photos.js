var express = require('express');
var router = express.Router();
var multer  = require('multer');
var fs  = require('fs');

var upload = multer({dest: '../pictures/'});
var type = upload.single('profilePicture');
var institution_table = require("../models/portfolio");

router.get('/image/:id', function(req, res, next) {
  var idphotographer = req.params.id;
  institution_table.findOne({
    where: { idphotographer: idphotographer }
  }).then(photos => {
    if(photos){
      var img = fs.readFileSync("'../pictures/'"+ photos.profilePicture);
      res.writeHead(200, { 'Content-Type': 'image/jpeg' });
      res.end(img, 'binary');
    }
  }).catch(function(error){
    console.log("Whoops!, Something happened...");    
    console.log(error);   
  }); 
});


router.post('/uploadPicture/:id', type, function(req, res, next) {
  var idphotographer = req.params.id;
  institution_table.findOne({
    where: { idphotographer: idphotographer }
  }).then(photos => {
    if(photos){
      photos.profilePicture = req.file.filename;
      photos.save();
      console.log(req.file);
      console.log(req.files);
      if (!req.files) {
        res.send('No files were uploaded.');
        return;
      }
      
    }else{      
    }
  }).catch(function(error){
    console.log("Whoops!, Something happened...");    
    console.log(error);   
  }); 
});


module.exports = router;
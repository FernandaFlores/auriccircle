var express = require('express');
var router = express.Router();
var sequelize = require("../models/rawSQL");
var rest = require("../models/rest");
var sha256=require('sha256');
var userTable = require("../models/example_users");




router.get("/ages",function(req,res,next){
console.log("ages");
var result = [ ['Ages', 'Qty'] ];
 	var ageQuery = "select age div 10 as 'between', count(id) as qty from example_users group by age div 10";//cambiar example_users a users
 	sequelize.query(ageQuery, { type: sequelize.QueryTypes.SELECT})
	.then(rows => {	    
		console.log(rows);
		for(rowIndex in rows){
			row = rows[rowIndex];
			var inf1=parseInt(rowIndex)+1;
			var inferior=inf1*10;
			var superior=inf1*10+9;
			console.log(rowIndex);
			console.log(inf1);
			console.log(inferior);
			result.push([inferior+'-'+superior, row.qty]);
		}
		res.json(result);
	});


});

router.get("/gender",function(req,res,next){
console.log("genders");
var result = [ ['Gender', 'Qty'] ];
 	var genderQuery = "select gender, count(1) as qty from example_users group by gender";//cambiar example_users a users
 	sequelize.query(genderQuery, { type: sequelize.QueryTypes.SELECT})
	.then(rows => {	    
		console.log(rows);
		for(rowIndex in rows){
			row = rows[rowIndex];
			result.push([row.gender, row.qty]);
		}
		res.json(result);
	});
});


module.exports = router;
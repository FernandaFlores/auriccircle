var mysql_host = "localhost";
var Sequelize = require('sequelize');
var sequelize = new Sequelize('AuricDB', 'root', 'somerlay15',{
	host: mysql_host,
	dialect: 'mysql'
});


var reservation = sequelize.define ('reservations',{
	"id"				:{type: Sequelize.INTEGER, primaryKey:true, autoIncrement: true},
	"name"				:{type: Sequelize.STRING},
	"place"				:{type: Sequelize.STRING},
	"iduser"			:{type: Sequelize.INTEGER},
	"idphotographer"	:{type: Sequelize.INTEGER},
	"country"			:{type: Sequelize.STRING},
	"city"				:{type: Sequelize.STRING},
	"type"				:{type: Sequelize.INTEGER},
	"date"				:{type: Sequelize.STRING},
    "total"				:{type: Sequelize.STRING},
    "paymentid": { type: Sequelize.INTEGER},
    "payerId" : { type: Sequelize.STRING },
	"payerToken" : { type: Sequelize.STRING },
    "paypalCreate": { type: Sequelize.JSON },
    "paypalCreateResponse": { type: Sequelize.JSON },
    "payPalPaymentID": { type: Sequelize.STRING },
    "qty" : { type: Sequelize.STRING },
    "created_at" : { type: Sequelize.DATE },
    "status" : { type: Sequelize.STRING,  defaultValue: Sequelize.NOW }
});

reservation.sync();//nombre la variable
module.exports = reservation;
var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var sessions = require('express-session'); 
var mailer=require('express-mailer');
var index = require('./routes/index');
var users = require('./routes/users');
var reservations = require('./routes/reservations');
var photographers = require('./routes/photographers');
var portfolios = require('./routes/portfolio');
var instagram = require('./routes/instagram');
var photos = require('./routes/photos');
var categories=require('./routes/categories');
var photocats=require('./routes/photocats');
var analytics=require('./routes/analitics');




var app = express();
//allow cross side domain requests
var allowCrossDomain = function(req, res, next) {
   res.header('Access-Control-Allow-Origin', '*');
   res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
   res.header('Access-Control-Allow-Headers', 'Content-Type');

   next();
}

// view engine setup
//app.set('views', path.join(__dirname, '/views'));
app.set('views', __dirname + '/views');
app.set('view engine', 'jade');


//MAILER
mailer.extend(app,{
from: 'auriccircle@gmail.com',
host:'smtp.gmail.com',
secureConnection: true,
port: 465,
transportMethod:'SMTP',
auth:{
  user:'auriccircle@gmail.com',
  pass:'golden2358'
}
});


// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(allowCrossDomain);
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
//SESIONES
app.use(sessions({
  //cookie: { secure: false },
 cookieName: 'session',
 // resave: true,
 // saveUninitialized: true, 
 secret: 'secreto',
 duration: 24 * 60 * 60 * 1000, // ms
 activeDuration: 1000 * 60 * 5
}));
//DIRECCIONES RUTAS
app.use('/', index);
app.use('/app', express.static("../Front"));
app.use('/users', users);
app.use('/photocats',photocats);
app.use('/categories',categories);
app.use('/reservations', reservations);
app.use('/photographers', photographers);
app.use('/portfolio', portfolios);
app.use('/instagram', instagram);
app.use('/photos', photos);
app.use('/analitics', analytics);




// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
 // res.render('error');
});

module.exports = app;
